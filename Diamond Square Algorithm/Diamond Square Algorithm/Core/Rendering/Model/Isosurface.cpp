#include "Isosurface.h"
#include "../../../MarchingCubes/MCTable.h"
using namespace Core::Rendering::Model;
using namespace Core::Rendering;

Isosurface::Isosurface()
{
	/*Initialise the world bounding box*/
	world.positions[0] = Vec3(0.0f, 0.0f, 0.0f);
	world.positions[1] = Vec3(0.0f, (float)worldY, 0.0f);
	world.positions[2] = Vec3((float)worldX, (float)worldY, 0.0f);
	world.positions[3] = Vec3((float)worldX, 0.0f, 0.0f);
	world.positions[4] = Vec3(0.0f, 0.0f, (float)worldZ);
	world.positions[5] = Vec3(0.0f, (float)worldY, (float)worldZ);
	world.positions[6] = Vec3((float)worldX, (float)worldY, (float)worldZ);
	world.positions[7] = Vec3((float)worldX, 0.0f, (float)worldZ);

	/*initialise the particles and scalar field*/
	for (unsigned int i = 0; i < (unsigned)numParticles; i++)
	{
		particles[i] = Particle_Isosurface(rnd_isosurface(7.0f), rnd_isosurface(7.0f), rnd_isosurface(7.0f),
			rnd_isosurface((0.01f + 0.01f)), rnd_isosurface((0.01f + 0.01f)), rnd_isosurface((0.01f + 0.01f)),
			rnd_isosurface(1.0f));
	}

	for (unsigned int i = 0; i < (unsigned)scalarX+1; i++)
	{
		for (unsigned int j = 0; j < (unsigned)scalarY+1; j++)
		{
			for (unsigned int k = 0; k < (unsigned)scalarZ+1; k++)
			{
				scalarField[i][j][k].x = scalarFieldOffsetX * i;
				scalarField[i][j][k].y = scalarFieldOffsetY * j;
				scalarField[i][j][k].z = scalarFieldOffsetZ * k;
			}
		}
	}
}

Isosurface::~Isosurface()
{
	//is going to be deleted in Models.cpp (Inheritance)
}

void Isosurface::Create()
{
	/*Perform the Particle Potential method on all of the vertices of the scalar field*/
	for (unsigned int t = 0; t < (unsigned)scalarX + 1; t++)
	{
		for (unsigned int o = 0; o < (unsigned)scalarY + 1; o++)
		{
			for (unsigned int h = 0; h < (unsigned)scalarZ + 1; h++)
			{
				scalarField[t][o][h].value = particlePotential(scalarField[t][o][h], particles, numParticles);
			}
		}
	}

	/*Get the triangles formed from the marching cubes method*/
	triangles = marchingCubes();

	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	
	/*Add vertices here*/
	/*for (unsigned int i = 0; i < (unsigned)numOfTriangles; i++)
	{
		vertices.push_back(VertexFormat(Vec3(triangles[i].position[0]), Vec4(1, 0, 0, 1)));
		vertices.push_back(VertexFormat(Vec3(triangles[i].position[1]), Vec4(0, 1, 0, 1)));
		vertices.push_back(VertexFormat(Vec3(triangles[i].position[2]), Vec4(0, 0, 1, 1)));
	}*/

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * (numOfTriangles * 3), &vertices[0], GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Triangles) * numOfTriangles, &triangles[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Triangles), (void*)0);
	//glEnableVertexAttribArray(1);
	//glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::color)));
	glBindVertexArray(0);

	this->vao = vao;
	this->vbos.push_back(vbo);

	/*set up camera transformations*/
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(50.0, 1.0, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 15.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

/*the marching cubes method*/
Triangles* Isosurface::marchingCubes(void)
{
	/*Initialise the triangles array for the isosurface*/
	Triangles* t = new Triangles[scalarX * scalarY * scalarZ * 3];
	numOfTriangles = int(0);

	for (unsigned int i = 0; i < (unsigned)scalarX; i++)
	{
		for (unsigned int j = 0; j < (unsigned)scalarY; j++)
		{
			for (unsigned int k = 0; k < (unsigned)scalarZ; k++)
			{
				/*Create a Cube from the points in the scalar field*/
				Vec3 verts[8];
				verts[0] = scalarField[i][j][k];
				verts[1] = scalarField[i+1][j][k];
				verts[2] = scalarField[i+1][j+1][k];
				verts[3] = scalarField[i][j+1][k];
				verts[4] = scalarField[i][j][k+1];
				verts[5] = scalarField[i+1][j][k+1];
				verts[6] = scalarField[i+1][j+1][k+1];
				verts[7] = scalarField[i][j+1][k+1];

				/*check all the cube vertices isovalue against the minimum threshold*/
				int cubeIndex = int(0);
				for (unsigned int h = 0; h < (unsigned)8; h++)
				{
					if (verts[h].value <= MIN_THRESHOLD_ISOSURFACE)
					{
						cubeIndex |= (1 << h);
					}
				}

				/*check if its complete inside or outside the table*/
				if (!edgeTable[cubeIndex]) continue;

				/*search for intersections*/
				Vec3 intersections[12];

				if (edgeTable[cubeIndex] & 1) intersections[0] = linearInterp(verts[0], verts[1], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 2) intersections[1] = linearInterp(verts[1], verts[2], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 4) intersections[2] = linearInterp(verts[2], verts[3], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 8) intersections[3] = linearInterp(verts[3], verts[0], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 16) intersections[4] = linearInterp(verts[4], verts[5], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 32) intersections[5] = linearInterp(verts[5], verts[6], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 64) intersections[6] = linearInterp(verts[6], verts[7], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 128) intersections[7] = linearInterp(verts[7], verts[4], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 256) intersections[8] = linearInterp(verts[0], verts[4], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 512) intersections[9] = linearInterp(verts[1], verts[5], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 1024) intersections[10] = linearInterp(verts[2], verts[6], (float)MIN_THRESHOLD_ISOSURFACE);
				if (edgeTable[cubeIndex] & 2048) intersections[11] = linearInterp(verts[3], verts[7], (float)MIN_THRESHOLD_ISOSURFACE);

				/*now is time to build the isosurface using the tritable*/
				for (unsigned int y = 0; (unsigned)triTable[cubeIndex][y] != -1; y += 3)
				{
					t[numOfTriangles].position[0] = intersections[triTable[cubeIndex][y + 2]];
					t[numOfTriangles].position[1] = intersections[triTable[cubeIndex][y + 1]];
					t[numOfTriangles].position[2] = intersections[triTable[cubeIndex][y]];

					/*work out the normals for each triangle*/
					
					/*a triangle has been formed, increment the triangle count*/
					numOfTriangles++;
				}
			}
		}
	}

	/*free all wasted space*/
	Triangles* realT = new Triangles[numOfTriangles];
	for (unsigned int i = 0; i < (unsigned)numOfTriangles; i++)
	{
		realT[i] = triangles[i];
	}
	delete[] t;

	return realT;
}

/*Linear Interpolation method*/
Vec3 Isosurface::linearInterp(Vec3& p1, Vec3& p2, float minValue)
{
	Vec3 temp;

	if (p1.value != p2.value)
		temp = p1 + (p2 - p1) * (minValue - p1.value) / (p2.value - p1.value);
	else
		temp = p1;
	return temp;
}

void Isosurface::Update()
{
	/*Perform particle updates*/
	for (unsigned int i = 0; i < (unsigned)numParticles; i++)
	{
		/*Apply collision detection for the particles*/
		if (particles[i].position.x >= (worldX - particles[i].radius) - 0.0001f)
		{
			particles[i].velocity.x = -particles[i].velocity.x;
		}
		if (particles[i].position.y >= (worldX - particles[i].radius) - 0.0001f)
		{
			particles[i].velocity.y = -particles[i].velocity.y;
		}
		if (particles[i].position.z >= (worldX - particles[i].radius) - 0.0001f)
		{
			particles[i].velocity.z = -particles[i].velocity.z;
		}
		if (particles[i].position.x <= (0 + particles[i].radius) + 0.0001f)
		{
			particles[i].velocity.x = -particles[i].velocity.x;
		}
		if (particles[i].position.y <= (0 + particles[i].radius) + 0.0001f)
		{
			particles[i].velocity.y = -particles[i].velocity.y;
		}
		if (particles[i].position.z <= (0 + particles[i].radius) + 0.0001f)
		{
			particles[i].velocity.z = -particles[i].velocity.z;
		}
		/*move the particles*/
		particles[i].position += particles[i].velocity;
	}

	/*Perform the Particle Potential method on all of the vertices of the scalar field*/
	for (unsigned int i = 0; i < (unsigned)scalarX + 1; i++)
	{
		for (unsigned int j = 0; j < (unsigned)scalarY + 1; j++)
		{
			for (unsigned int k = 0; k < (unsigned)scalarZ + 1; k++)
			{
				scalarField[i][j][k].value = particlePotential(scalarField[i][j][k], particles, numParticles);
			}
		}
	}

	/*Get the triangles formed from the marching cubes method*/
	triangles = marchingCubes();
}

void Isosurface::Draw()
{
	GLenum err = glGetError();

	std::cerr << "OpenGL error: " << gluErrorString(err) << std::endl;

	/*glUseProgram(program);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);*/

	glPushMatrix();
	glPointSize(3.0);
	glBegin(GL_POINTS);

	for (unsigned int i = 0; i < (unsigned)numParticles; i++)
	{
		glVertex3f(particles[i].position.x, particles[i].position.y, particles[i].position.z);
	}

	glEnd();
	glPopMatrix();

	glPushMatrix();
	glBegin(GL_TRIANGLES);

	for (int i = 0; i < numOfTriangles; i++)
	{
		//set the normals
		//glNormal3f(triangles[i].normal.x, triangles[i].normal.y, triangles[i].normal.z);

		for (int j = 0; j < 3; j++)
		{
			glVertex3f(triangles[i].position[j].x, triangles[i].position[j].y, triangles[i].position[j].z);
		}
	}
	glEnd();
	glPopMatrix();
}

/*Particle Potential method*/
float Isosurface::particlePotential(Vec3& p1, Particle_Isosurface *p2, int numBalls)
{
	float temp = 0;

	for(unsigned int i = 0; i < (unsigned)numBalls; i++)
	{
		temp += 1 / (((p1.x - p2[i].position.x) * (p1.x - p2[i].position.x)) + ((p1.y - p2[i].position.y) * (p1.y - p2[i].position.y)) + ((p1.z - p2[i].position.z) * (p1.z - p2[i].position.z)));
	}

	return temp;
}
