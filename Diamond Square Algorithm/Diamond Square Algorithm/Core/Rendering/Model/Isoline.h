#pragma once
#include "Model.h"
#include "../Vector.h"
#include "../../Init/WindowInfo.h"
#include <list>

namespace Core
{
	namespace Rendering
	{
		namespace Model
		{
			/*user define iso value thresholds*/
#define MAX_THRESHOLD_ISOLINE 1.01f
#define MIN_THRESHOLD_ISOLINE 0.99f

#define rnd_isoline(x) (x * rand() / RAND_MAX)

			const int windowWidth = 800;
			const int windowHeight = 600;

			/*the particle structure*/
			struct Particle_Isoline
			{
				Vec2 position;
				Vec2 velocity;
				int radius;

				/*equation for giving the particle a charge*/
				float particleCharge(float x0, float y0)
				{
					return(radius / sqrt((x0 - position.x) * (x0 - position.x) + (y0 - position.y) * (y0 - position.y)));
				}
			};

			class Isoline : public virtual Model
			{
			private:

				/*The number of particles that are in the scene*/
				static const int numParticles = 10;

				/*The particles array*/
				Particle_Isoline particles[numParticles];

				/*The pixel buffer*/
				std::vector<VertexFormat> pixelBuffer;

			public:
				Isoline();
				~Isoline();
				void Create();
				virtual void Update() override final;
				virtual void Draw() override final;

				/*Particle potential method*/
				void particlePotential(void);

				/*Set up the viewport for 2d*/
				void setUp2D(void);
			};

		}
	}
}