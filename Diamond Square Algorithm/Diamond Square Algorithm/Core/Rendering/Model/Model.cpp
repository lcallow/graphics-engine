#include "Model.h"

using namespace Core::Rendering::Model;

Model::Model(){}

Model::~Model()
{
	Destroy();
}

void Model::Draw()
{
	//this will be overridden again
}

void Model::Update()
{
	//this will be overridden again
}

void Model::SetProgram(GLuint program)
{
	this->program = program;
}

GLuint Model::GetVao() const
{
	return vao;
}

const std::vector<GLuint> Model::GetVbos() const
{
	return vbos;
}

void Model::Destroy()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers((GLsizei)vbos.size(), &vbos[0]);
	vbos.clear();
}