#include "Isoline.h"

using namespace Core::Rendering::Model;
using namespace Core::Rendering;
using namespace Core::Init;

Isoline::Isoline()
{
	for (unsigned int i = 0; i < (unsigned)numParticles; i++)
	{
		particles[i].position.x = (float)rnd_isoline(windowWidth);
		particles[i].position.y = (float)rnd_isoline(windowHeight);
		particles[i].velocity.x = (float)(rnd_isoline(10) + 1);
		particles[i].velocity.y = (float)(rnd_isoline(10) + 1);
		particles[i].radius = rnd_isoline(50);

		if (particles[i].position.x < particles[i].radius)
		{
			particles[i].position.x += particles[i].radius;
		}
	}
}

Isoline::~Isoline()
{
}

void Isoline::Draw(void)
{
	//glUseProgram(program);
	//glBindVertexArray(vao);
	//glDrawArrays(GL_POINTS, 0, pixelBuffer.size());
	glVertex2i(pixelBuffer[100].pos2.x, pixelBuffer[100].pos2.y);
}

void Isoline::Update(void)
{
	/*for (unsigned int i = 0; i < (unsigned)numParticles; i++)
	{
	if (particles[i].position.x  < (0 + particles[i].radius))
	{
	particles[i].position.x++;
	particles[i].velocity.x *= -1;
	}
	if (particles[i].position.x  > (windowWidth - particles[i].radius))
	{
	particles[i].position.x--;
	particles[i].velocity.x *= -1;
	}
	if (particles[i].position.y  < (0 + particles[i].radius))
	{
	particles[i].position.y++;
	particles[i].velocity.y *= -1;
	}
	if (particles[i].position.y  > (windowHeight - particles[i].radius))
	{
	particles[i].position.y--;
	particles[i].velocity.y *= -1;
	}

	particles[i].position.x += particles[i].velocity.x;
	particles[i].position.y += particles[i].velocity.y;
	}

	particlePotential();*/
}

void Isoline::setUp2D(void)
{
	//tell opengl how to convert from coordinates to pixel values
	glViewport(0, 0, windowWidth, windowHeight);

	glMatrixMode(GL_PROJECTION);

	//set the camera perspective
	glLoadIdentity();
	gluPerspective(45.0, (double)windowWidth / (double)windowHeight, 1.0, 200.0);
	//glOrtho(0, windowWidth, windowHeight, 0, -10, 10);
}

void Isoline::Create(void)
{
	particlePotential();

	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * pixelBuffer.size(), &pixelBuffer[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::color)));
	glBindVertexArray(0);
	this->vao = vao;
	this->vbos.push_back(vbo);
}

void Isoline::particlePotential(void)
{
	/*The energy value*/
	float temp = 0;

	for (unsigned int i = 0; i < (unsigned)windowWidth; i++)
	{
		for (unsigned int j = 0; j < (unsigned)windowHeight; j++)
		{
			/*reset the enery value per pixel on screen*/
			temp = 0;

			/*check every particle in the scene*/
			for (unsigned int h = 0; h < (unsigned)numParticles; h++)
			{
				temp += particles[h].particleCharge(i, j);
			}

			/*Check the enery value based on the user defined threshold values*/
			if (temp >= MIN_THRESHOLD_ISOLINE && temp <= MAX_THRESHOLD_ISOLINE)
			{
				/*Add a pixel to the pixel buffer array*/
				pixelBuffer.push_back(VertexFormat(Vec3((float)i, (float)j, 0.0f), Vec4(1, 0, 0, 1)));
			}
		}
	}
}
