#include "Flag.h"


using namespace Core::Rendering::Model;
using namespace Core::Rendering;

Flag::Flag()
{
}


Flag::~Flag()
{
}

void Flag::Draw(void)
{

}

void Flag::Update(void)
{

}

void Flag::Create(void)
{

}

void Flag::init_mesh(struct flag_mesh *outMesh, struct flag_vertex const *vertexData, GLsizei vertexCount, GLushort const *elementData, GLsizei elementCount, GLenum hint)
{
	glGenBuffers(1, &outMesh->vertex_buffer);
	glGenBuffers(1, &outMesh->element_buffer);
	outMesh->element_count = elementCount;

	glBindBuffer(GL_ARRAY_BUFFER, outMesh->vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(struct flag_vertex), vertexData, hint);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, outMesh->element_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementCount * sizeof(GLushort), elementData, GL_STATIC_DRAW);
}

void Flag::calculateFlagVertex(struct flag_vertex *v, GLfloat s, GLfloat t, GLfloat time)
{
	GLfloat sgrad[3] = {
		1.0f + 0.5f*(0.0625f + 0.03125f*sinf((GLfloat)M_PI * time)) * t * (t - 1.0f),
		0.0f,
		0.125f * (sinf(1.5f * (GLfloat)M_PI * (time * s)) + s * cosf(1.5f * (GLfloat)M_PI * (time + s)) * (1.5f * (GLfloat)M_PI))
	},
	tgrad[3] = {
		-(0.0625f + 0.03125f * sinf((GLfloat)M_PI * time)) * (1.0f - s) * (2.0f * t - 1.0f),
		0.75f,
		0.0f
	};

		v->position[0] = s - (0.0625f + 0.03125f * sinf((GLfloat)M_PI * time)) * (1.0f - 0.5f * s) * t * (t - 1.0f);
		v->position[1] = 0.75f * t - 0.375f;
		v->position[2] = 0.125f * (s * sinf(1.5f * (GLfloat)M_PI * (time + s)));
		v->position[3] = 0.0f;

		Vec3 temp;
		temp.crossProduct(v->normal, tgrad, sgrad);
		temp.normalize(v->normal);
		v->normal[3] = 0.0f;
}


