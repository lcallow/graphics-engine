#pragma once
#include "Model.h"
#include "../Vector.h"

namespace Core
{
	namespace Rendering
	{
		namespace Model
		{
			class Triangle : public virtual Model
			{
			public:
				Triangle();
				~Triangle();

				void Create();
				virtual void Update() override final;
				virtual void Draw() override final;
			};
		}
	}
}