//Model.h

/*This is the models header file*/

#pragma once
#include <vector>
#include "../GameObject.h"

namespace Core
{
	namespace Rendering
	{
		namespace Model
		{
			class Model : public virtual Core::Rendering::GameObject
			{
			public:
				Model();
				virtual ~Model();

				//methods from the interface
				virtual void Draw() override;
				virtual void Update() override;
				virtual void SetProgram(GLuint shaderName) override;
				virtual void Destroy() override;

				virtual GLuint GetVao() const override;
				virtual const std::vector<GLuint> GetVbos() const override;

			protected:
				GLuint vao;
				GLuint program;
				std::vector<GLuint> vbos;
			};
		}
	}
}