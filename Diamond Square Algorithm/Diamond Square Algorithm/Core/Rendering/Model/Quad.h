#pragma once
#include "Model.h"
#include "../Vector.h"

namespace Core
{
	namespace Rendering
	{
		namespace Model
		{
			class Quad : public virtual Model
			{
			public:
				Quad();
				~Quad();

				void Create();
				virtual void Draw() override final;
				virtual void Update() override final;
			};
		}
	}
}

