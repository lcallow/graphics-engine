#pragma once
#include "Model.h"
#include "../Vector.h"

namespace Core
{
	namespace Rendering
	{
		namespace Model
		{

			/*The threshold for the marching cubes algorithm, this value is checked against the isovalue*/
			#define MAX_THRESHOLD_ISOSURFACE 1.00
			#define MIN_THRESHOLD_ISOSURFACE 0.98

			#define rnd_isosurface( x) ( x * rand() / RAND_MAX)

			/*struct for drawing the isosurface*/
			struct Triangles
			{
				Vec3 position[3];
				Vec3 normal;
			};

			/*For forming the isosurface, checking against the scalar field*/
			struct Particle_Isosurface
			{
				Vec3 position;
				Vec3 velocity;
				float radius;

				Particle_Isosurface(){}

				Particle_Isosurface(float x, float y, float z,
					float vx, float vy, float vz, float r)
				{
					position.x = x;
					position.y = y;
					position.z = z;
					velocity.x = vx;
					velocity.y = vy;
					velocity.z = vz;
					radius = r;
				}

				void operator =(const Particle_Isosurface& other)
				{
					position.x = other.position.x;
					position.y = other.position.y;
					position.z = other.position.z;
					velocity.x = other.velocity.x;
					velocity.y = other.velocity.y;
					velocity.z = other.velocity.z;
					radius = other.radius;
				}
			};

			struct BoundingBox
			{
				Vec3 positions[8];

				BoundingBox() {}

				BoundingBox(Vec3 v1, Vec3 v2, Vec3 v3, Vec3 v4,
					Vec3 v5, Vec3 v6, Vec3 v7, Vec3 v8)
				{
					positions[0] = v1;
					positions[1] = v2;
					positions[2] = v3;
					positions[3] = v4;
					positions[4] = v5;
					positions[5] = v6;
					positions[6] = v7;
					positions[7] = v8;
				}
			};

			class Isosurface : public virtual Model
			{
			public:

				/*length of the scalar field*/
				static const int scalarX = 25, scalarY = 25, scalarZ = 25;

				/*World box lengths*/
				static const int worldX = 10, worldY = 10, worldZ = 10;

				/*offset for scaling the scalar field*/
				float scalarFieldOffsetX = 0.4f, scalarFieldOffsetY = 0.4f, scalarFieldOffsetZ = 0.4f;

				/*the scalar field*/
				Vec3 scalarField[scalarX+1][scalarY+1][scalarZ+1];

				/*the total number of particles*/
				static const int numParticles = 2;

				/*The particles*/
				Particle_Isosurface particles[numParticles];

				/*The world bounding box*/
				BoundingBox world;

				/*The triangles that is going to form the isosurface*/
				Triangles* triangles;

				/*The number of triangles*/
				int numOfTriangles;

				/*method for computing the isovalue for every vertex in the scalar field*/
				float particlePotential(Vec3& p1, Particle_Isosurface *p2, int numBalls);

				/*Linear interpolation for finding the exact position of the inersection with the surface*/
				Vec3 linearInterp(Vec3& p1, Vec3& p2, float minValue);

				/*The marching cubes method*/
				Triangles* marchingCubes(void);

				Isosurface();
				~Isosurface();

				void Create();
				virtual void Update() override final;
				virtual void Draw() override final;


			};
		}
	}
}

