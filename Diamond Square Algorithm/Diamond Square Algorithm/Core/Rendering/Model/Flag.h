#pragma once
#include "Model.h"
#include "../Vector.h"
#include "../../Init/WindowInfo.h"

namespace Core
{
	namespace Rendering
	{
		namespace Model
		{

			#ifndef M_PI
			#define M_PI 3.141592653589793
			#endif

			/*Flag mesh setup*/
			struct flag_mesh
			{
				GLuint vertex_buffer, element_buffer;
				GLsizei element_count;
				GLuint texture;
			};

			struct flag_vertex
			{
				GLfloat position[4];
				GLfloat normal[4];
				GLfloat texcoord[2];
				GLfloat shininess;
				GLubyte specular[4];
			};

			class Flag : public virtual Model
			{
			public:
				Flag();
				~Flag();
				void Create();
				virtual void Update() override final;
				virtual void Draw() override final;

				void init_mesh(struct flag_mesh *out_mesh, struct flag_vertex const *vertexData, GLsizei vertexCount, GLushort const *elementData, GLsizei elementCount, GLenum hint);
				void initBackgroundMesh(struct flag_mesh *outMesh);
				void updateFlagMesh(struct flag_mesh const *mesh, struct flag_vertex *vertexData, GLfloat time);
				void calculateFlagVertex(struct flag_vertex *v, GLfloat s, GLfloat t, GLfloat time);
				struct flag_vertex* initFlagMesh(struct flag_mesh *outMesh);

			};
		}
	}
}
