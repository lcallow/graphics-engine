//Vector.h

/*This is the custom made vector library*/
#pragma once
#include <math.h>

namespace Core
{
	namespace Rendering
	{
		struct Vec2
		{
			float x, y;

			Vec2(){}

			Vec2(float x, float y)
			{
				this->x = x;
				this->y = y;
			}

			//copy constructor
			Vec2(const Vec2& other)
			{
				this->x = other.x;
				this->y = other.y;
			}

			//Assign constructor
			void operator=(const Vec2& other)
			{
				this->x = other.x;
				this->y = other.y;
			}

			//magnitude method for working out the length of the vector
			float magnitude(void)
			{
				return sqrt(this->x * this->x + this->y * this->y);
			}

			//magnitude squared
			float magnitudeSquared(void)
			{
				return sqrt(this->x * this->x * this->y * this->y);
			}

			//dot product method
			float dotProduct(const Vec2& other)
			{
				return this->x * other.x + this->y * other.y;
			}

			void operator *=(const float scale)
			{
				this->x *= scale;
				this->y *= scale;
			}

			//method for returning the cosine between 2 vectors, using the dot product
			float dotProductCos(Vec2& other)
			{
				float theta;

				theta = dotProduct(other) / (magnitude() * other.magnitude());

				return theta;
			}
		};

		struct Vec3
		{
			float x, y, z;

			/*This is used for Marching Cubes Implementation*/
			float value;

			Vec3(){}

			Vec3(float x, float y, float z)
			{
				this->x = x;
				this->y = y;
				this->z = z;
			}

			//copy constructor
			Vec3(const Vec3& other)
			{
				this->x = other.x;
				this->y = other.y;
				this->z = other.z;
			}

			//assign constructor
			void operator=(const Vec3 other)
			{
				this->x = other.x;
				this->y = other.y;
				this->z = other.z;
			}

			/*return the inverse of the vector*/
			void operator-()
			{
				this->x = -this->x;
				this->y = -this->y;
				this->z = -this->z;
			}

			/*Vector maths overloading operators*/
			void operator-=(const Vec3 other)
			{
				this->x -= other.x;
				this->y -= other.y;
				this->z -= other.z;
			}
			void operator+=(const Vec3 other)
			{
				this->x += other.x;
				this->y += other.y;
				this->z += other.z;
			}
			void operator/=(const float c)
			{
				this->x /= c;
				this->y /= c;
				this->z /= c;
			}

			/*Overloading operators that return a vector*/
			Vec3 operator-(const Vec3 other)
			{
				Vec3 temp;

				temp.x = this->x - other.x;
				temp.y = this->y - other.y;
				temp.z = this->z - other.z;

				return temp;
			}

			Vec3 operator+(const Vec3 other)
			{
				Vec3 temp;

				temp.x = this->x + other.x;
				temp.y = this->y + other.y;
				temp.z = this->z + other.z;

				return temp;
			}

			Vec3 operator*(const float c)
			{
				Vec3 temp;

				temp.x = this->x * c;
				temp.y = this->y * c;
				temp.z = this->z * c;

				return temp;
			}

			Vec3 operator/(const float c)
			{
				Vec3 temp;

				temp.x = this->x / c;
				temp.y = this->y / c;
				temp.z = this->z / c;

				return temp;
			}

			//magnitude method for computing the length of the vector
			float magnitude()
			{
				return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
			}

			/*Same magnitude method but on the vector being passed in*/
			GLfloat magnitude(GLfloat *v)
			{
				return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
			}

			//method for computing the magnitude squared
			float magnitudeSquared()
			{
				return sqrt(this->x * this->x * this->y * this->y * this->z * this->z);
			}

			//dot product method for a vec3
			float dotProduct(const Vec3& other)
			{
				return this->x * other.x + this->y * other.y + this->z * other.z;
			}

			//dot product for returning the cosine between 2 vectors
			float dotProductCos(Vec3& other)
			{
				float theta;

				theta = dotProduct(other) / (magnitude() * other.magnitude());

				return theta;
			}

			/*cross product, which takes in the outputted vector*/
			void crossProduct(GLfloat *outResult, GLfloat *u, GLfloat *v)
			{
				outResult[0] = u[1] * v[2] - u[2] * v[1];
				outResult[1] = u[2] * v[0] - u[0] * v[2];
				outResult[2] = u[0] * v[1] - u[1] * v[0];
			}

			/*Normalize a vector, output vector as one of the parameters*/
			void normalize(GLfloat *outVector)
			{
				GLfloat rlen = 1.0f / magnitude(outVector);
				outVector[0] *= rlen;
				outVector[1] *= rlen;
				outVector[2] *= rlen;
			}
		};

		struct Vec4
		{
			float x, y, z, w;

			Vec4(){}

			Vec4(float x, float y, float z, float w)
			{
				this->x = x;
				this->y = y;
				this->z = z;
				this->w = w;
			}

			//copy constructor
			Vec4(const Vec4& other)
			{
				this->x = other.x;
				this->y = other.y;
				this->z = other.z;
				this->w = other.w;
			}

			//assign constructor
			void operator=(const Vec4& other)
			{
				this->x = other.x;
				this->y = other.y;
				this->z = other.z;
				this->w = other.w;
			}
		};

		/*FUTURE PROJECT, IMPLEMENT CUSTOM TEMPLATE VECTOR
		FOR NOW USE STD::VECTOR*/

		////Template Vec3
		//template <class T>
		//class TVec3
		//{
		//private:
		//	std::vector<T> elements;
		//public:

		//};
	}
}