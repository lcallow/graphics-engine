//VertexFormat.h

/*This is the Vertex format header file, this header filer holds a struct for a vertex to have a position and color*/
#pragma once
#include "Vector.h"

namespace Core
{
	namespace Rendering
	{
		struct VertexFormat
		{
			Vec3 position;
			Vec4 color;
			Vec2 pos2;

			VertexFormat(const Vec3 &pos, const Vec4 &iColor)
			{
				position = pos;
				color = iColor;
			}

			VertexFormat(const Vec2 &pos, const Vec4 &iColor)
			{
				pos2 = pos;
				color = iColor;
			}

			VertexFormat() {}
		};
	}
}