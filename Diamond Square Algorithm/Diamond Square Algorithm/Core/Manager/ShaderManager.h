//ShaderManager.h

/*This is the shader manager header file, this will handle all there is to do with the shaders for the graphics engine*/

#pragma once
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include "../../Dependencies/glew/glew.h"
#include "../../Dependencies/freeglut/freeglut.h"

namespace Manager
{
	class ShaderManager
	{
	public:
		ShaderManager();
		~ShaderManager();

		//method for creating a program
		void CreateProgram(const std::string& shaderName,
			const std::string& vertexShaderName,
			const std::string& fragmentShaderName);
		
		//method for getting a certain shader via its name
		static const GLuint GetShader(const std::string&);

	private:
		//method for reading in a shader from a file
		std::string ReadShader(const std::string& filename);

		//methof for creating a shader to be referenced by a program
		GLuint CreateShader(GLenum shaderType,
			const std::string& vertexShaderName,
			const std::string& fragmentShaderName);

		//handler for multiple GLSL programs
		static std::map<std::string, GLuint> programs;
	};
}



