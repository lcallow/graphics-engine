#include "ModelManager.h"

using namespace Manager;
using namespace Core::Rendering;

ModelManager::ModelManager()
{
	//Triangle game object
	//Model::Triangle* triangle = new Model::Triangle();
	//triangle->SetProgram(ShaderManager::GetShader("colorShader"));
	//triangle->Create();
	//gameModelList["triangle"] = triangle;

	/*Generate the Isosurface*/
	/*Model::Isosurface* iso = new Model::Isosurface();
	iso->SetProgram(ShaderManager::GetShader("colorShader"));
	iso->Create();
	gameModelList["isosurface"] = iso;*/

	/*Generate the quad*/
	/*Model::Quad* quad = new Model::Quad();
	quad->SetProgram(ShaderManager::GetShader("colorShader"));
	quad->Create();
	gameModelList["quad"] = quad;*/

	/*Generate the isoline*/
	Model::Isoline* isoline = new Model::Isoline();
	isoline->SetProgram(ShaderManager::GetShader("colorShader"));
	isoline->Create();
	//isoline->setUp2D();
	gameModelList["isoline"] = isoline;
}


ModelManager::~ModelManager()
{
	//auto - its a map iterator
	for (auto model : gameModelList)
	{
		delete model.second;
	}
	gameModelList.clear();
}

void ModelManager::DeleteModel(const std::string& gameModelName)
{
	GameObject* model = gameModelList[gameModelName];
	model->Destroy();
	gameModelList.erase(gameModelName);
}

const GameObject& ModelManager::GetModel(const std::string& gameModelName) const
{
	return (*gameModelList.at(gameModelName));
}

void ModelManager::Update()
{
	//auto its a map iterator
	for (auto model : gameModelList)
	{
		model.second->Update();
	}
}

void ModelManager::Draw()
{
	for (auto model : gameModelList)
	{
		model.second->Draw();
	}
}