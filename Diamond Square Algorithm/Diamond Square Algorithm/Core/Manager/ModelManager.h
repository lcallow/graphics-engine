#pragma once
#include <map>
#include "ShaderManager.h"
#include "../Rendering/Model/Triangle.h"
#include "../Rendering/GameObject.h"
#include "../Rendering/Model/Isosurface.h"
#include "../Rendering/Model/Quad.h"
#include "../Rendering/Model/Isoline.h"

using namespace Core::Rendering;

namespace Manager
{
	class ModelManager
	{
	public:
		ModelManager();
		~ModelManager();

		void Draw();
		void Update();
		void DeleteModel(const std::string& gameModelName);
		const GameObject& GetModel(const std::string& gameModelName) const;

	private:
		std::map<std::string, GameObject*> gameModelList;
	};
}

