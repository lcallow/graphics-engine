//SceneManager.h

/*This is the scene manager header file, this manager will control the listener*/

#pragma once
#include "ShaderManager.h"
#include "ModelManager.h"
#include "../../Core/Init/Listener.h"
namespace Manager
{
	class SceneManager : public Core::Init::Listener
	{
	public:
		SceneManager();
		~SceneManager();

		virtual void notifyBeginFrame();
		virtual void notifyDisplayFrame();
		virtual void notifyEndFrame();
		virtual void notifyReshape(int width, int height, int previousWidth, int previousHeight);
	private:
		Manager::ShaderManager* shaderManager;
		Manager::ModelManager* modelManager;
	};
}

