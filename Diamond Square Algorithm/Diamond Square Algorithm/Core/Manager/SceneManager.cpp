//SceneManager.cpp
#include "SceneManager.h"

using namespace Manager;

SceneManager::SceneManager()
{
	glEnable(GL_DEPTH_TEST);

	shaderManager = new ShaderManager();
	shaderManager->CreateProgram("colorShader", "Shaders\\vertexShader.glsl", "Shaders\\fragmentShader.glsl");

	//create the Model Manager
	modelManager = new ModelManager();
}


SceneManager::~SceneManager()
{
	delete shaderManager;
	delete modelManager;
}

void SceneManager::notifyBeginFrame()
{
	//the triangle for this example is static and doesnt update anything
	//but we need to call the update function every frame for future models
	//including the current triangle
	modelManager->Update();
}

void SceneManager::notifyDisplayFrame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	modelManager->Draw();
}

void SceneManager::notifyEndFrame()
{

}

void SceneManager::notifyReshape(int width, int height, int previousWidth, int previousHeight)
{

}
