//InitGlut.h
#include "InitGlut.h"


using namespace Core::Init;

/*Make sure that static attributes are visible in cpp*/
Core::Init::Listener* InitGlut::listener = NULL;
Core::Init::WindowInfo InitGlut::windowInformation;

void InitGlut::init(const Core::Init::WindowInfo& window,
	const Core::Init::ContextInfo& context,
	const Core::Init::FrameBufferInfo& frameBufferInfo)
{
	windowInformation = window;

	//need to create some fake arguments
	int fakeargc = 1;
	char *fakeargv[] = { "fake", NULL };
	glutInit(&fakeargc, fakeargv);

	if (context.core)
	{
		glutInitContextVersion(context.majorVersion, context.minorVersion);
		glutInitContextProfile(GLUT_CORE_PROFILE);
	}
	else
	{
		//version doesnt matter when in compatibility mode
		glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	}

	/*Init the glut window*/
	glutInitDisplayMode(frameBufferInfo.flags);
	glutInitWindowPosition(window.positionX, window.positionY);
	glutInitWindowSize(window.width, window.height);
	glutCreateWindow(window.name.c_str());

	std::cout << "Glut Initialised!" << std::endl;

	/*The following callbacks are used for rendering*/
	glutIdleFunc(idleCallback);
	glutCloseFunc(closeCallback);
	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);

	/*Initialise GLEW*/
	Init::InitGlew::Init();

	/*Clean Up*/
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	/*Method for displaying some opengl info, needs context and window info*/
	printOpenGLInfo(window, context);
}

/*Starts the rendering loop*/
void InitGlut::run()
{
	std::cout << "Glut:\t Start Running" << std::endl;
	glutMainLoop();
}

void InitGlut::close()
{
	std::cout << "Glut:\t Finished!" << std::endl;
	glutLeaveMainLoop();
}

/*idle callback function*/
void InitGlut::idleCallback(void)
{
	/*for the moment do nothing*/
	glutPostRedisplay();
}

/*display function callback*/
void InitGlut::displayCallback(void)
{
	/*Check for null*/
	if (listener)
	{
		listener->notifyBeginFrame();
		listener->notifyDisplayFrame();

		glutSwapBuffers();

		listener->notifyEndFrame();
	}
}

/*reshape callback function*/
void InitGlut::reshapeCallback(int width, int height)
{
	if (windowInformation.isReshapeable == true)
	{
		if (listener)
		{
			listener->notifyReshape(width, height, windowInformation.width, windowInformation.height);
		}
		windowInformation.width = width;
		windowInformation.height = height;
	}
}

/*close callback function*/
void InitGlut::closeCallback(void)
{
	close();
}

/*enter fullscreen function*/
void InitGlut::enterFullscreen()
{
	glutFullScreen();
}

/*exit fullscreen function*/
void InitGlut::exitFullscreen()
{
	glutLeaveFullScreen();
}

void InitGlut::printOpenGLInfo(const Core::Init::WindowInfo& windowInfo,
	const Core::Init::ContextInfo& contextInfo)
{
	const unsigned char* renderer = glGetString(GL_RENDERER);
	const unsigned char* vendor = glGetString(GL_VENDOR);
	const unsigned char* version = glGetString(GL_VERSION);

	std::cout << "*******************************************************" << std::endl;
	std::cout << "GLUT:Initialise" << std::endl;
	std::cout << "GLUT:\tVendor :" << vendor << std::endl;
	std::cout << "GLUT:\tRenderer :" << renderer << std::endl;
	std::cout << "GLUT:\tVersion :" << version << std::endl;
	std::cout << "GLUT:\tInitial window is '" << windowInfo.name << "', with dimensions (" << windowInfo.width
		<< "X" << windowInfo.height;
	std::cout << ") starts at (" << windowInfo.positionX << "X" << windowInfo.positionY;
	std::cout << ") and " << ((windowInfo.isReshapeable) ? "is" : "is not ") << " redimensionable" << std::endl;
	std::cout << "GLUT:\tInitial Framebuffer contains double buffers for" << std::endl;

	std::cout << "GLUT:\t OpenGL context is " << contextInfo.majorVersion << "." << contextInfo.minorVersion;
	std::cout << " and profile is " << ((contextInfo.core) ? "core" : "compatibility") << std::endl;

	std::cout << "*****************************************************************" << std::endl;
}

/*Set the listener*/
void InitGlut::setListener(Core::Init::Listener*& list)
{
	listener = list;
}
