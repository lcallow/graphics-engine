//Listener.h

/*This is the listener header which will be used to bridge the gap between the managers and 
Init sections of the graphics engine*/

#pragma once
namespace Core
{
	namespace Init
	{
		class Listener
		{
		public:
			virtual ~Listener() = 0;

			//drawing functions
			virtual void notifyBeginFrame() = 0;
			virtual void notifyDisplayFrame() = 0;
			virtual void notifyEndFrame() = 0;
			virtual void notifyReshape(int width, int height,
				int previousWidth, int previousHeight) = 0;
		};

		inline Listener::~Listener()
		{
			//Implementation of pure virtual deconstructor
		}
		
	}
}