//InitGlew.h

/*This is the InitGlew header file which will initialise glew for the graphics engine*/

#pragma once
#include <iostream>
#include "../../Dependencies/glew/glew.h"
#include "../../Dependencies/freeglut/freeglut.h"

namespace Core
{
	namespace Init
	{
		class InitGlew
		{
			public:
				static void Init();
		};
	}
}

