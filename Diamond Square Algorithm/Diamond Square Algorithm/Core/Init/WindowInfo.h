//WindowInfo.h

/*This header file will hold all the information for the Window for OpenGL*/

#pragma once
#include <string>

namespace Core
{
	namespace Init
	{
		struct WindowInfo
		{
			//Hold the name of the window
			std::string name;
			//the width and height of the window
			int width, height;
			//the position of the window on screen
			int positionX, positionY;
			//Whether the window is reshapeable or not
			bool isReshapeable;

			//Default Constructor
			WindowInfo()
			{
				name = "Liam Callow's Graphics Engine";
				width = 800;
				height = 600;
				positionX = 300;
				positionY = 300;
				isReshapeable = true;
			}

			WindowInfo(std::string name, int positionX, int positionY, int height,
				int width, bool isReshapeable)
			{
				this->name = name;
				this->height = height;
				this->width = width;
				this->positionX = positionX;
				this->positionY = positionY;
				this->isReshapeable = isReshapeable;
			}

			//Copy Constructor
			WindowInfo(const WindowInfo& other)
			{
				this->height = other.height;
				this->width = other.width;
				this->isReshapeable = other.isReshapeable;
				this->positionX = other.positionX;
				this->positionY = other.positionY;
				this->name = other.name;
			}

			//Assign Constructor
			void operator=(const WindowInfo& other)
			{
				this->height = other.height;
				this->width = other.width;
				this->positionX = other.positionX;
				this->positionY = other.positionY;
				this->name = other.name;
				this->isReshapeable = other.isReshapeable;
			}
		};
	}
}