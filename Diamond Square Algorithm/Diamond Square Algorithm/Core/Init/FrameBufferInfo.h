//FrameBufferInfo.h

/*This header file will hold all the information for the display mode*/

#pragma once
#include "../../Dependencies/glew/glew.h"
#include "../../Dependencies/freeglut/freeglut.h"

namespace Core
{
	namespace Init
	{
		struct FrameBufferInfo
		{
			unsigned int flags;
			//to be able to enable or disable on the fly
			bool msaa;

			//Default Constructor
			FrameBufferInfo()
			{
				flags = GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH;
				msaa = false;
			}

			FrameBufferInfo(bool color, bool depth, bool stencil, bool msaa)
			{
				//this is a must
				flags = GLUT_DOUBLE;

				if (color)
					flags |= GLUT_RGBA | GLUT_ALPHA;
				if (depth)
					flags |= GLUT_DEPTH;
				if (stencil)
					flags |= GLUT_STENCIL;
				if (msaa)
					flags |= GLUT_MULTISAMPLE;
				this->msaa = msaa;
			}

			//Copy Constuctor
			FrameBufferInfo(const FrameBufferInfo& other)
			{
				this->flags = other.flags;
				this->msaa = other.msaa;
			}

			//Assign Constructor
			void operator=(const FrameBufferInfo& other)
			{
				this->flags = other.flags;
				this->msaa = other.msaa;
			}
		};
	}
}