//ContextInfo.h

/* This header files holds all the information about the context for OpenGL, eg Version*/

namespace Core
{
	namespace Init
	{
		//openGL Versions
		struct ContextInfo
		{
			//major version refers to the first number, and minor refers to the second number
			//for a version of OpenGL.
			int majorVersion, minorVersion;
			//Bool for which context mode to use, Core or Compatibility, in Compatibility mode,
			//the engine can gain access to deprecated OpenGL methods pre 3.0 whilst still using a higher
			//GL version
			bool core;

			//default constructor
			ContextInfo()
			{
				//set the version of OpenGL to 3.3 by default
				majorVersion = 3;
				minorVersion = 3;
				core = true;
			}

			ContextInfo(int majorVersion, int minorVersion, bool core)
			{
				this->majorVersion = majorVersion;
				this->minorVersion = minorVersion;
				this->core = core;
			}

			//Copy Constructor
			ContextInfo(const ContextInfo& other)
			{
				this->core = other.core;
				this->majorVersion = other.majorVersion;
				this->minorVersion = other.minorVersion;
			}

			//Assign Constructor
			void operator=(const ContextInfo& other)
			{
				this->core = other.core;
				this->majorVersion = other.majorVersion;
				this->minorVersion = other.minorVersion;
			}
		};
	}
}