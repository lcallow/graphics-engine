//InitGlut.h

/*This is the InitGlut header file which will initialise glut for the graphics engine*/

#pragma once
#include "ContextInfo.h"
#include "FrameBufferInfo.h"
#include "WindowInfo.h"
#include <iostream>
#include "InitGlew.h"
#include "Listener.h"

namespace Core
{
	namespace Init
	{
		class InitGlut
		{
		public:
			static void init(const Core::Init::WindowInfo& window,
				const Core::Init::ContextInfo& context,
				const Core::Init::FrameBufferInfo& frameBufferInfo);

			static void run();
			static void close();

			void enterFullscreen();
			void exitFullscreen();

			//method for printing opengl info out
			static void printOpenGLInfo(const Core::Init::WindowInfo& windowInfo,
				const Core::Init::ContextInfo& context);

			//set the listener
			static void setListener(Core::Init::Listener*& listener);

		private:
			
			static Core::Init::Listener* listener;
			static Core::Init::WindowInfo windowInformation;
			
			static void idleCallback(void);
			static void displayCallback(void);
			static void reshapeCallback(int width, int height);
			static void closeCallback();
		};
	}
}
