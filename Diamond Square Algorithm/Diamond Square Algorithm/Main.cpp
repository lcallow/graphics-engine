#pragma once
#include "Core\Init\InitGlut.h"
#include "Core\Manager\SceneManager.h"

using namespace Core::Init;

int main(int argc, char** argv)
{
	WindowInfo window(std::string("Liam Callow's Graphics Engine"), 400, 200, 800, 600, true);

	ContextInfo context(4, 5, true);

	FrameBufferInfo frameBufferInfo(true, true, true, true);

	InitGlut::init(window, context, frameBufferInfo);

	Listener* scene = new Manager::SceneManager();
	InitGlut::setListener(scene);

	InitGlut::run();

	delete scene;

	return 0;
}
