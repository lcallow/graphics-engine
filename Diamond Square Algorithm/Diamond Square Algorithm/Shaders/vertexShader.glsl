#version 450 core

//set the first input on location(index) 0, in_position is out attribute
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec4 in_color;

out vec4 color;

void main(void)
{
	color = in_color;
	gl_Position = vec4(in_position, 1);
}